const helpers = {};

helpers.isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) return next();
    req.flash('error_msg', 'Not Authorized.');
    res.redirect('/users/signin');
};

helpers.isLogged = (req, res, next) => {
    if (!req.isAuthenticated()) return next();
    res.redirect('/users/logout');
};

module.exports = helpers;