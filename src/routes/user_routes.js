const express = require('express');
const router = express.Router();
const axios = require('axios');

const User = require('../models/Users');
const { isLogged } = require('../helpers/auth');

const passport = require('passport');

router.get('/users/signin', isLogged, (req, res) => {
    res.render('users/signin');
});

router.post('/users/signin', passport.authenticate('local', {
    successRedirect: '/users/home',
    failureRedirect: '/users/signin',
    failureFlash: true
}));



router.get('/users/signup', isLogged, (req, res) => {
    res.render('users/signup');
});

router.post('/users/signup', async (req, res) => {
    const { name, email, password, confirmPassword } = req.body;
    const errors = [];

    if(name.length == 0)
        errors.push({ text: 'Please insert your name' });  
    if (password !== confirmPassword)
        errors.push({ text: 'Passwords do not match.'});
    if (password.length < 4)
        errors.push({ text: 'Password must be at least 4 characters'});
    if (errors.length > 0) {
        res.render('users/signup', { errors, name, email, password, confirmPassword });
    } else {
        const emailUser = await User.findOne({ email: email });
        if(emailUser) {
            req.flash('error_msg', 'The email is already in use');
            res.redirect('/users/signup');
        }

        axios.post('http://smartid.dis.eafit.edu.co/persons', {
            "alias": name,
            "kind": "natural"
        }).then(async (res) => {
            const corda_id = res.data;
            const newUser = new User({ name, email, password, corda_id });
            newUser.password = await newUser.encryptPassword(password);
            console.log(newUser);
            await newUser.save();
        });
        req.flash('success_msg', 'You are registered');
        res.redirect('/users/signin');
    }
});

router.get('/users/logout', (req, res) => {
    req.logout();
    res.redirect('/users/signin');
});

module.exports = router;