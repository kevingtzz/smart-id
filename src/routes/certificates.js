const express = require('express');
const router = express.Router();
const axios = require('axios');

const { isAuthenticated } = require('../helpers/auth');
let certificates = [];
let filtered_certificates = [];

router.get('/users/home/:tag?', isAuthenticated, (req, res) => {

    if (req.query.tag === undefined) {
    
        axios.get(`http://smartid.dis.eafit.edu.co/persons/${req.user.corda_id}/certificates`)
            .then((ress) => {
                try {
                    certificates = ress.data;
                    filtered_certificates = certificates;
                    res.render('users/home', { filtered_certificates });
    
                } catch (error){
                    console.log(error);
                    res.render('users/home');
                }
            });

    } else {
        if (req.query.tag == "overall") {
            filtered_certificates = certificates;
            res.render('users/home', { filtered_certificates });
        } else {
            filtered_certificates = [];
            certificates.forEach(element => {
                tag = element.tags;
                console.log(tag);
                if(tag[0] === req.query.tag) filtered_certificates.push(element);
            });
            res.render('users/home', { filtered_certificates });
        }
    }
});

module.exports = router;