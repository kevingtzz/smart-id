const overall_button = document.getElementById('overall');
const financial_button = document.getElementById('financial');
const clinical_button = document.getElementById('clinical');
const academic_button = document.getElementById('academic');
const properties_button = document.getElementById('properties');
const share_btn = document.getElementById('share_btn');


//if(academic_button.classList.contains('active')) console.log('ok');//overall_button.classList.remove('active');
console.log(academic_button.classList.contains('active'));


overall_button.addEventListener('click', () => {
    overall_button.classList.add('active');
    financial_button.classList.remove('active');
    clinical_button.classList.remove('active');
    academic_button.classList.remove('active');
    properties_button.classList.remove('active');
});

financial_button.addEventListener('click', () => {
    financial_button.classList.add('active');
    overall_button.classList.remove('active');
    clinical_button.classList.remove('active');
    academic_button.classList.remove('active');
    properties_button.classList.remove('active');
});

clinical_button.addEventListener('click', () => {
    clinical_button.classList.add('active');
    overall_button.classList.remove('active');
    financial_button.classList.remove('active');
    academic_button.classList.remove('active');
    properties_button.classList.remove('active');
});

academic_button.addEventListener('click', () => {
    academic_button.classList.add('active');
    overall_button.classList.remove('active');
    clinical_button.classList.remove('active');
    financial_button.classList.remove('active');
    properties_button.classList.remove('active');
});

properties_button.addEventListener('click', () => {
    properties_button.classList.add('active');
    overall_button.classList.remove('active');
    clinical_button.classList.remove('active');
    academic_button.classList.remove('active');
    financial_button.classList.remove('active');
});

share_btn.addEventListener('click', () => {
    share_btn.style.color = "red";
});
