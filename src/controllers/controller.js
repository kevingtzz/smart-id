const config = require('../../config');
const path = require('path');
const controller = {};

controller.get_loggin = (req, res) => {
    res.render(path.join(config.AppPath, '/src/views/index.html'));
}

module.exports = {
    controller
}